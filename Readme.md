## cloner le repertoire

`git clone URL_REPO`

## change directory

`cd CLONED_FOLDER`

## Create our python virtual environment with conda

`conda create -n NAMEOFTHEENV python=3.8`

## Activate the environment

`conda activate NAMEOFTHEENV`

## Install dependencies

`pip install jupyter xgboost seaborn pandas scikit-learn`

## Saving your dependencies in requirement.txt

`pip freeze > requirements.txt`

## Install dependencies thanks to the requirements file located in the git repo

`pip install -r requirements.txt`